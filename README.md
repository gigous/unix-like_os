# Unix-like Operating System

An operating system created from scratch (Contributors: A. Krishnamoorthy, R. Hoagland). Written in C and x86 assembly. Can load and execute ELF programs which the user can specify in the command line.
